<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/kirim-email', 'EmailController@index');

Route::get('/surat', 'SuratController@index');

Route::get('/surat/hapus/{id}','SuratController@hapus');

Route::get('/surat/edit/{id}','SuratController@edit');

Route::post('/surat/update','SuratController@update');

Route::get('/surat/tambah','SuratController@tambah');

Route::post('/surat/store','SuratController@store');

Route::get('view-data', 'AuthorizationController@viewData');
Route::get('create-data', 'AuthorizationController@createData');
Route::get('edit-data', 'AuthorizationController@editData');
Route::get('update-data', 'AuthorizationController@updateData');
Route::get('delete-data', 'AuthorizationController@deleteData');
Route::get('/upload', 'UploadController@upload');
Route::post('/upload/proses', 'UploadController@proses_upload');

Route::get('/upload/hapus/{id}', 'UploadController@hapus');
Route::get('/upload/downloadfile/{id}', 'UploadController@download')->name('downloadfile');

Route::get('/logout', function(){
   Auth::logout();
   return Redirect::to('login');
});
Route::get('/', function(){
   return Redirect::to('login');
});
Route::get('/register','UserRegisterController@index');
Route::post('/register/store','UserRegisterController@store');
<!DOCTYPE html>
<html>
<head>
	<title>SIES · SURAT</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

	<div class="row">
		<div class="container">

			<h2 class="text-center my-5">Tambah Data Surat</h2>

	<a class="btn btn-info" href="/surat">Kembali</a>
	
	<br/>
	<br/>

	<form action="/surat/store" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		 <div class="form-row">
    		<div class="form-group col-md-6">
			<label>Nama Pengirim</label><br> 
			<input type="text" name="nama_pengirim" class="form-control" placeholder="masukkan nama pengirim" required="required"> <br/>
		</div>

		<div class="form-group col-md-6">
			<label>Tanggal Surat</label><br>
			<input type="date" name="tgl_surat" class="form-control" placeholder="HH/BB/TTTT" required="required"> <br/>
		</div>
		</div>

		<div class="form-row">
    		<div class="form-group col-md-6">
			<label>No. Surat</label><br>
			<input type="text" name="no_surat" class="form-control" placeholder="nomor surat" required="required"> <br/>
		</div>

		<div class="form-group col-md-6">
			<label>Hal Surat</label><br>
			<input type="text" name="hal_surat" class="form-control" placeholder="perihal surat" required="required"> <br/>
		</div>
		</div>
		 
		<div class="form-group">
			<label>Diteruskan Kepada</label><br>
			<input type="text" name="diteruskan_kpda" class="form-control" placeholder="bagian tujuan" required="required"> <br/>
		</div>
		
		<div class="form-row">
    	<div class="form-group col-md-6">
			<label>Tanggal Terima Surat</label><br>
			<input type="date" name="tgl_terima" class="form-control" placeholder="HH/BB/TTTT" required="required"> <br/>
		</div>

		<div class="form-group col-md-6">
			<label>Tanggal Agenda</label><br>
			<input type="date" name="tgl_agenda" class="form-control" placeholder="HH/BB/TTTT" required="required"> <br/>
		</div>
		</div>

		<div class="form-group">
			<label>No. Agenda</label><br>
			<input type="text" name="no_agenda" class="form-control" placeholder="nomor agenda" required="required"> <br/>
		</div>

		<div class="form-group">
			<label>Isi Disposisi</label><br>
			<textarea name="isi_disposisi" class="form-control" placeholder="masukkan isi disposisi" required="required"></textarea> <br/>
		</div>

		<input class="btn btn-success" type="submit" value="Simpan Data">
		<a class="btn btn-secondary" href="/upload">Upload Berkas</a>

	</form>

</div>
</div>
</body>
</html>
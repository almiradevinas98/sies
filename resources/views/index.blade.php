<!DOCTYPE html>
<html>
<head>
	<title>SIES · SURAT</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

	<div class="row">
		<div class="container">

			<h2 class="text-center my-5">SURAT</h2>

	<table class="table table-bordered table-striped">
		<thead>
			<th>No.</th>
			<th>Nama Pengirim</th>
			<th>Tanggal Surat</th>
			<th>No. Surat</th>
			<th>Hal Surat</th>
			<th>Diteruskan Kepada</th>
			<th>Tanggal Terima Surat</th>
			<th>Tanggal Agenda</th>
			<th>No. Agenda</th>
			<th>Isi Disposisi</th>
			<th>Opsi</th>
		</thead>
		@foreach($surat as $s)
		<tbody>
			<tr>
			<td>{{ $s->id_surat}}.</td>
			<td>{{ $s->nama_pengirim }}</td>
			<td>{{ $s->tgl_surat }}</td>
			<td>{{ $s->no_surat }}</td>
			<td>{{ $s->hal_surat }}</td>
			<td>{{ $s->diteruskan_kpda }}</td>
			<td>{{ $s->tgl_terima }}</td>
			<td>{{ $s->tgl_agenda }}</td>
			<td>{{ $s->no_agenda }}</td>
			<td>{{ $s->isi_disposisi }}</td>
			<td>
				<a class="btn btn-info" href="/surat/edit/{{ $s->id_surat }}">Edit</a>
				<a class="btn btn-dark" href="/surat/hapus/{{ $s->id_surat }}">Hapus</a>
			</td>
			</tr>
		</tbody>
		@endforeach
	</table>
	<td>
		<a class="btn btn-success" href="/surat/tambah">Tambah Data Surat</a>
	</td>
</div>
</div>
</body>
</html>
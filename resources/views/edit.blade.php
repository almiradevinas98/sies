<!DOCTYPE html>
<html>
<head>
	<title>SIES · SURAT</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

	<div class="row">
		<div class="container">

			<h2 class="text-center my-5">Edit Data Surat</h2>

	<a class="btn btn-info" href="/surat">Kembali</a>
	
	<br/>
	<br/>

	@foreach($surat as $s)
	<form action="/surat/update" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $s->id_surat }}"> <br/>

		<div class="form-row">
    		<div class="form-group col-md-6">
			<label>Nama Pengirim</label><br> 
			<input type="text" name="nama_pengirim" class="form-control" value="{{ $s->nama_pengirim }}" placeholder="masukkan nama pengirim" required="required"> <br/>
		</div>

		<div class="form-group col-md-6">
			<label>Tanggal Surat</label><br>
			<input type="date" name="tgl_surat" class="form-control"  value="{{ $s->tgl_surat }}" placeholder="HH/BB/TTTT" required="required"> <br/>
		</div>
		</div>

		<div class="form-row">
    		<div class="form-group col-md-6">
			<label>No. Surat</label><br>
			<input type="text" name="no_surat" class="form-control" value="{{ $s->no_surat }}" placeholder="nomor surat" required="required"> <br/>
		</div>

		<div class="form-group col-md-6">
			<label>Hal Surat</label><br>
			<input type="text" name="hal_surat" class="form-control" value="{{ $s->hal_surat }}" placeholder="perihal surat" required="required"> <br/>
		</div>
		</div>

		<div class="form-group">
			<label>Diteruskan Kepada</label><br>
			<input type="text" name="diteruskan_kpda" class="form-control" value="{{ $s->diteruskan_kpda }}" required="required"> <br/>
		</div>
		
		<div class="form-row">
    	<div class="form-group col-md-6">
			<label>Tanggal Terima Surat</label><br>
			<input type="date" name="tgl_terima" class="form-control" value="{{ $s->tgl_terima }}" required="required"> <br/>
		</div>

		<div class="form-group col-md-6">
			<label>Tanggal Agenda</label><br>
			<input type="date" name="tgl_agenda" class="form-control" value="{{ $s->tgl_agenda }}" required="required"> <br/>
		</div>
		</div>

		<div class="form-group">
			<label>No. Agenda</label><br>
			<input type="text" name="no_agenda" class="form-control" value="{{ $s->no_agenda }}" required="required"> <br/>
		</div>

		<div class="form-group">
			<label>Isi Disposisi</label><br>
			<textarea name="isi_disposisi" class="form-control" required="required">{{ $s->isi_disposisi }}</textarea> <br/>
		</div>
		
		<input class="btn btn-success" type="submit" value="Simpan Data">
		<a class="btn btn-secondary" href="/upload">Upload Berkas</a>
	
	</form>
	@endforeach
		

</body>
</html>
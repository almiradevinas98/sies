<!DOCTYPE html>
<html lang="en">
<head>
    <title>SIES | Login Page</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
    <link rel="icon" type="image/png" href="loginfile/images/icons/favicon.ico"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="loginfile/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="loginfile/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="loginfile/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="loginfile/vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="loginfile/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="loginfile/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="loginfile/vendor/select2/select2.min.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="loginfile/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="loginfile/css/util.css">
    <link rel="stylesheet" type="text/css" href="loginfile/css/main.css">

<style>
.center {
  display: block;
  margin-left: auto;
  margin-right: auto;
   margin-bottom: 10%;
   width:180px; 
}
</style>
<!--===============================================================================================-->
</head>
<body>
    
                   
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form method="POST" action="{{ route('login') }}">
                        @csrf
                    <span class="login100-form-title p-b-26">
                        SIES
                    </span>
                    <img src="logo.png" class="center">
                    </span>

                           

                    <div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
                        <input id="email" type="email" class="input100 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required  autofocus>
                        <span class="focus-input100" data-placeholder="Email"></span>
                        @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>


                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <span class="btn-show-pass">
                            <i class="zmdi zmdi-eye"></i>
                        </span>
                        <input class="input100" type="password" name="password" required autocomplete="current-password">
                        <span class="focus-input100" data-placeholder="Password"></span>
                          @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>

                    <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <button class="login100-form-btn" type="submit" >
                                  {{ __('Login') }}
                            </button>
                        </div>
                    </div>

                    <div class="text-center p-t-115">
                        <span class="txt1">
                            Forgot your password? 
                            @if (Route::has('password.request'))
                                    <a class="btn btn-link txt2" href="{{ route('password.request') }}">
                                        {{ __('Click here') }}
                                    </a>
                                @endif
                        </span>
                           
                     
                    </div>
                </form>
            </div>
        </div>
    </div>
    

    <div id="dropDownSelect1"></div>
    
<!--===============================================================================================-->
    <script src="loginfile/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
    <script src="loginfile/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
    <script src="loginfile/vendor/bootstrap/js/popper.js"></script>
    <script src="loginfile/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
    <script src="loginfile/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
    <script src="loginfile/vendor/daterangepicker/moment.min.js"></script>
    <script src="loginfile/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
    <script src="loginfile/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
    <script src="loginfile/js/main.js"></script>


</body>
</html>

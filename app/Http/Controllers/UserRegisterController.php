<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class UserRegisterController extends Controller
{
     public function index()
    {    
        return view('Admin/RegisterUser');
    }
     public function store(Request $request)
    {    
    	$hashed_password = password_hash($request->password, PASSWORD_DEFAULT);
    	DB::table('users')->insert([
		'name' => $request->nama,
		'email' => $request->email,
		'password' => $hashed_password,
		'role_id' => $request->level
	]);
	
	return redirect('/home');
    }
}


<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use File;
use Response;

class FileController extends Controller
{
    function download_gambar($gambar){
    	$files = Storage::files("public");
    	$gambar=array();
    	foreach ($files as $key => $value) {
    		$value= str_replace("public/","",$value);
    		array_push($gambar,$value);
    	}
	return view('show', ['gambar' => $gambar]);
    }
}
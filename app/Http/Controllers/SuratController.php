<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SuratController extends Controller
{
    public function index()
    {
    	// mengambil data dari table surat
    	$surat = DB::table('surat')->get();
 
    	// mengirim data surat ke view index
    	return view('index',['surat' => $surat]);
    }

    public function hapus($id)
	{
		// menghapus data surat berdasarkan id yang dipilih
		DB::table('surat')->where('id_surat',$id)->delete();
			
		// alihkan halaman ke halaman surat
		return redirect('/surat');
	}

	public function edit($id)
	{
		// mengambil data surat berdasarkan id yang dipilih
		$surat = DB::table('surat')->where('id_surat',$id)->get();
		// passing data surat yang didapat ke view edit.blade.php
		return view('edit',['surat' => $surat]);
	}

	public function update(Request $request)
	{
		// update data surat
		DB::table('surat')->where('id_surat',$request->id)->update([
			'nama_pengirim' => $request->nama_pengirim,
			'tgl_surat' => $request->tgl_surat,
			'no_surat' => $request->no_surat,
			'hal_surat' => $request->hal_surat,
			'diteruskan_kpda' => $request->diteruskan_kpda,
			'tgl_terima' => $request->tgl_terima,
			'tgl_agenda' => $request->tgl_agenda,
			'no_agenda' => $request->no_agenda,
			'isi_disposisi' => $request->isi_disposisi
		]);
		// alihkan halaman ke halaman surat
		return redirect('/surat');
	}

	public function tambah()
	{
 
		// memanggil view tambah
		return view('tambah');
 
	}

	// method untuk insert data ke table surat
	public function store(Request $request)
	{
		// insert data ke table surat
		DB::table('surat')->insert([
			'id_surat' => $request->id_surat,
			'nama_pengirim' => $request->nama_pengirim,
			'tgl_surat' => $request->tgl_surat,
			'no_surat' => $request->no_surat,
			'hal_surat' => $request->hal_surat,
			'diteruskan_kpda' => $request->diteruskan_kpda,
			'tgl_terima' => $request->tgl_terima,
			'tgl_agenda' => $request->tgl_agenda,
			'no_agenda' => $request->no_agenda,
			'isi_disposisi' => $request->isi_disposisi
		]);
		// alihkan halaman ke halaman surat
		return redirect('/surat');
	 
	}

}
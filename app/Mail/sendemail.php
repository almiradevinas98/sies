<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class sendemail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name)
    {
     $this->name = $name->all();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        Return $this->from('almiradevinas98@gmail.com')->view('email')->with(['nama' => $this->name]);
    }
}
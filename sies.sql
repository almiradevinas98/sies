-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 01 Mar 2021 pada 08.58
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sies`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `gambar`
--

CREATE TABLE `gambar` (
  `id` int(10) NOT NULL,
  `file` varchar(255) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gambar`
--

INSERT INTO `gambar` (`id`, `file`, `keterangan`, `created_at`, `updated_at`) VALUES
(2, '1606739266_icon_small.png', 'asdfghjkl', '2020-11-30 12:27:46', '2020-11-30 12:27:46'),
(5, '1609313251_unnamed (2).jpg', 'hhhhhh', '2020-12-30 07:27:32', '2020-12-30 07:27:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gambars`
--

CREATE TABLE `gambars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_10_25_144153_create_user_table', 1),
(5, '2020_10_25_162727_change_user_to_tbuser_table', 2),
(6, '2020_11_12_070851_create_roles_table', 3),
(7, '2020_11_12_071140_create_permissions_table', 3),
(8, '2020_11_12_071151_create_permission_role_table', 3),
(9, '2020_11_17_135047_create_gambars_table', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('almiradevinas98@gmail.com', '$2y$10$u0nO8O08IzVC1QBHiTweuO5/4BsCM2b29fbSlK8ZYxuL3Ow/3Rgqy', '2020-10-26 03:17:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(4, 'view data', '2020-11-12 01:06:20', '2020-11-12 01:06:20'),
(5, 'create data', '2020-11-12 01:06:20', '2020-11-12 01:06:20'),
(6, 'edit data', '2020-11-12 01:06:20', '2020-11-12 01:06:20'),
(7, 'update data', '2020-11-12 01:06:20', '2020-11-12 01:06:20'),
(8, 'delete data', '2020-11-12 01:06:20', '2020-11-12 01:06:20'),
(9, 'view data', '2020-11-12 01:07:56', '2020-11-12 01:07:56'),
(10, 'create data', '2020-11-12 01:07:56', '2020-11-12 01:07:56'),
(11, 'edit data', '2020-11-12 01:07:56', '2020-11-12 01:07:56'),
(12, 'update data', '2020-11-12 01:07:56', '2020-11-12 01:07:56'),
(13, 'delete data', '2020-11-12 01:07:56', '2020-11-12 01:07:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `permission_role`
--

CREATE TABLE `permission_role` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permission_role`
--

INSERT INTO `permission_role` (`id`, `created_at`, `updated_at`, `role_id`, `permission_id`) VALUES
(1, NULL, NULL, 1, 1),
(2, NULL, NULL, 1, 2),
(3, NULL, NULL, 1, 5),
(4, NULL, NULL, 2, 1),
(5, NULL, NULL, 2, 2),
(6, NULL, NULL, 2, 3),
(7, NULL, NULL, 2, 4),
(8, NULL, NULL, 3, 1),
(9, NULL, NULL, 3, 2),
(10, NULL, NULL, 3, 3),
(11, NULL, NULL, 3, 4),
(12, NULL, NULL, 3, 5),
(13, NULL, NULL, 1, 1),
(14, NULL, NULL, 1, 2),
(15, NULL, NULL, 1, 5),
(16, NULL, NULL, 2, 1),
(17, NULL, NULL, 2, 2),
(18, NULL, NULL, 2, 3),
(19, NULL, NULL, 2, 4),
(20, NULL, NULL, 3, 1),
(21, NULL, NULL, 3, 2),
(22, NULL, NULL, 3, 3),
(23, NULL, NULL, 3, 4),
(24, NULL, NULL, 3, 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`role_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-11-12 01:04:56', '2020-11-12 01:04:56'),
(2, 'staff', '2020-11-12 01:04:56', '2020-11-12 01:04:56'),
(3, 'ceo', '2020-11-12 01:04:56', '2020-11-12 01:04:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat`
--

CREATE TABLE `surat` (
  `id_surat` int(20) NOT NULL,
  `nama_pengirim` varchar(255) NOT NULL,
  `tgl_surat` date NOT NULL,
  `no_surat` varchar(255) NOT NULL,
  `hal_surat` varchar(255) NOT NULL,
  `diteruskan_kpda` varchar(255) NOT NULL,
  `tgl_terima` date NOT NULL,
  `tgl_agenda` date NOT NULL,
  `no_agenda` varchar(255) NOT NULL,
  `isi_disposisi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `surat`
--

INSERT INTO `surat` (`id_surat`, `nama_pengirim`, `tgl_surat`, `no_surat`, `hal_surat`, `diteruskan_kpda`, `tgl_terima`, `tgl_agenda`, `no_agenda`, `isi_disposisi`) VALUES
(3, 'zuzuzuzuzu', '2020-11-03', '123456789', 'mipan', 'asdfghjkl', '2020-11-04', '2020-11-04', '0987654321', 'asdfghjkl'),
(5, 'asdfghjkl', '2020-12-29', '1234567890', 'zxcvbnm', 'kepala kantor', '2020-12-30', '2020-12-30', '12345666', 'qwertyuiop');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbuser`
--

CREATE TABLE `tbuser` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'kasubag', 'lalalalala@gmail.com', NULL, '$2y$10$dqcGCu2BFp0U9hwasal19O.BW/BBp0oJKv37b0yxNZxVOBhEoD8iG', 'kasubag', NULL, '2020-10-25 09:47:42', '2020-10-25 09:47:42'),
(2, 'mira', 'almiradevinas98@gmail.com', NULL, '$2y$10$zyQ4d37iT43Aeief6iZQ8utR5YoiaI0UuGUyMx7Un4o/JFYAdF1/6', 'siebaglain', NULL, '2020-10-25 10:06:01', '2020-10-25 10:06:01'),
(3, 'miraae', 'miraagae@gmail.com', NULL, '$2y$10$uIdQQwb4LfBe5coS1uI5POLtLznk78xj0ggAdX3Gt3UTRZdvaAe/a', 'admin', 'GKmqoDDuf6zEoIxLh4MMDZfN7eRKyHYTQz09pmj85fNp12tM3DCwr5qa8mjh', '2020-10-26 03:31:52', '2020-12-30 00:24:20'),
(4, 'kk', 'kepalakantor@gmail.com', NULL, '$2y$10$iJ2AXd27kuM4MAzqXs3SyeHvfWHv2LS6.qEOqhh61jIEnTMqfOqvS', 'kepalakantor', NULL, '2020-11-11 23:55:10', '2020-11-11 23:55:10'),
(5, 'Mr. Junk', 'junk@gmail.com', NULL, '$2y$10$uKP7r1DS.Y.DctmdqkVQ9ejK.nH9WaDqF238hOIfZ6lh6ZPplBpZe', '1', NULL, '2020-11-12 01:07:57', '2020-11-12 01:07:57'),
(6, 'Mr. Jenkin', 'jenkin@gmail.com', NULL, '$2y$10$aozFXODXeil5FcHatqYRPemAx1MgnLjHzvZx0/F7o/UwlCWORWXwe', '2', NULL, '2020-11-12 01:07:57', '2020-11-12 01:07:57'),
(7, 'Miss. Puff', 'puff@gmail.com', NULL, '$2y$10$uB6aDHNnMGApOmAV39bA8uuZsX4xM2GKcKP3O9UUdmcShffqM9VgO', '3', NULL, '2020-11-12 01:07:57', '2020-11-12 01:07:57');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `gambar`
--
ALTER TABLE `gambar`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `gambars`
--
ALTER TABLE `gambars`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indeks untuk tabel `surat`
--
ALTER TABLE `surat`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indeks untuk tabel `tbuser`
--
ALTER TABLE `tbuser`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_username_unique` (`username`),
  ADD UNIQUE KEY `user_email_unique` (`email`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `gambar`
--
ALTER TABLE `gambar`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `gambars`
--
ALTER TABLE `gambars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `surat`
--
ALTER TABLE `surat`
  MODIFY `id_surat` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbuser`
--
ALTER TABLE `tbuser`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD CONSTRAINT `password_resets_ibfk_1` FOREIGN KEY (`email`) REFERENCES `users` (`email`);

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id`) REFERENCES `permission_role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
